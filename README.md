# Teste IsNewDigitalTech

Parabéns você foi aprovado para a segunda fase do processo seletivo de Desenvolvedor De Software Jr na IsNew Digital Tech.

Você terá dois dias para terminar e entregar esse teste. Seguem algumas instruções para realizar o teste:

Nesse teste, você deverá criar uma interface do zero, consumir nossa API.

O objetivo é criar um sistema no qual o usuário poderá adicionar os livros que estão na nossa base de dados em sua lista. Caso o livro não esteja disponível na base de dados, poderá adicionar.

## Regras do Teste

- Utilizar Vue ou Nuxt.js
- Consumir e utilizar API Targaryen:
  - Criar conta
  - Fazer login
  - Poder adicionar os livros da base de dados como queridos
  - Poder criar novos livros que não estão na base de dados

## Telas

A criação de telas pode ser conforme você (e sua imaginação) desejar, porém é preciso consumir/enviar dados para API, seja de registro de usuário ou de livros.

## Documentação da API

A documentação da API está disponível [nesse link](https://targaryen.isnewdtech.com.br/docs)

## Finalização do Teste

Abra um Merge Request nesse repositório com o seu código e listando os principais pontos que diferenciam o seu código.